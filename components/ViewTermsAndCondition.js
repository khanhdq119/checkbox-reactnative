import React, { Component } from 'react';
import {StyleSheet,Text,View,Linking} from 'react-native'
import customData from '../json/terms.json';

export default class ViewTermsAndCondition extends Component {
  render() {
    return (
        <View>
            <Text style={{fontSize:16}}>{customData.content}</Text>
            <Text style={{color: 'blue'}}
                onPress={()=> {this.setState({allowCheck:true}); Linking.openURL(customData.link)}}>
                  {customData.text_link}
            </Text>
        </View>
    );
  }
}