import React, { Component } from 'react';
import { 
  Alert, 
  Image, 
  Platform, 
  StyleSheet, 
  Text, 
  TouchableOpacity,
  TouchableHighlight, 
  View,
  Linking,
  Button
} from 'react-native';
import MyCheckBox from './MyCheckBox';
import ViewTermsAndCondition from './ViewTermsAndCondition';
import Modal from 'react-native-modal';
import { WebView } from 'react-native-webview';

class SelectedCheckboxes {
    constructor() {
      selectedCheckboxes = [];
    }
  
    addItem(option) {
      selectedCheckboxes.push(option);   
    }
  
    fetchArray() {
      return selectedCheckboxes;
    }
}
export default class TermAncCondition extends Component {
    state={
      showWebView:false,
      isModalVisible:false,
      allowCheck:false
    }
    toggleModal = () => {
      console.log("abbbbba");
      this.setState({
        showWebView: !this.state.showWebView,
        isModalVisible: !this.state.isModalVisible, 
        allowCheck: true   
      });
    };
    constructor() {
      super();
      CheckedArrObject = new SelectedCheckboxes();
    }
  
    render() {
      return (        
        <View style={styles.container}>
          <View style={[styles.row]}>
            <MyCheckBox
              keyValue={1}
              checked={this.state.checked}
              color="#3F50B5"
              labelColor="#000000"
              label=""
              value=""
              disabled={true}  
              checkedObjArr={CheckedArrObject} 
              changeState={this.state.allowCheck} />
            <ViewTermsAndCondition/>

          </View>
          <View style={styles.CheckboxContainer}>
            <Modal isVisible={this.state.isModalVisible}>
              <Button
                onPress={this.toggleModal}
                title="Close"
                color="#FFFFFF"
              />
              {
                (this.state.showWebView === true && 
                  <WebView
                    source={{ uri: "https://reactnative.dev/" }}
                    style={{flex: 1}}
                  />)     
              }
            </Modal>
          </View>
        </View>
        );
    }
}
   
const styles = StyleSheet.create( 
    {
      container: {
        flex: 1,
        padding: 20,  
        paddingTop: (Platform.OS === 'ios') ? 100 : 0
      },
      row: {
        flexDirection:"row",  
        alignItems: 'center',
        flexWrap: "wrap",      
      },
      fontSize: {
        fontSize: 16,
      },
      paddingLeft15: {
        paddingLeft: 15,
      },
      modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#000000',
        padding: 100
      },
      text: {
          color: '#3f2949',
          marginTop: 10
      },
      CheckboxContainer: {
        flex: 1,
        padding: 22,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: (Platform.OS === 'ios') ? 25 : 0
      }, 
});