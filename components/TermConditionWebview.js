import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import {Button, View,Alert,Modal} from 'react-native'

export default class TermConditionWebview extends Component {
  state={showWebView:true}

  renderWebView(){
    if  (this.state.showWebView === true){
      return(
        <WebView
          source={{ uri: "https://reactnative.dev/" }}
          style={{flex: 1}}
        />
      );
    }
    return null;
  }

  render() {
    return (
      <View style={{flex:1}}>
        <Button
          onPress={()=>this.setState({showWebView:false})}
          title="Close"
          color="#FFFFFF"
        />
      {this.renderWebView()}
      </View>
    );
  }
}