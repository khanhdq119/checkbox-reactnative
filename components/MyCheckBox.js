import React, { Component } from 'react';

import { 
  Alert, 
  Image, 
  Platform, 
  StyleSheet, 
  Text, 
  TouchableHighlight, 
  View,
  Linking,
  Button
} from 'react-native';
import PropTypes from 'prop-types';

export default class MyCheckBox extends Component {

    constructor() {
        super();
        this.state = { 
            checked: null 
        }
    }

    componentDidMount() {
        if (this.props.checked) {
            this.setState({ checked: true }, () => {
                this.props.checkedObjArr.addItem({
                    'key': this.props.keyValue,
                    'value': this.props.value,
                    'label': this.props.label
                });
            });
        } else {
            this.setState({ 
                checked: false
            });
        }
    }

    stateSwitcher(key, label, value) {
        if(this.props.changeState === true) {
            this.setState({ checked: !this.state.checked }, () => {
                if (this.state.checked) {
                    this.props.checkedObjArr.addItem({ 
                        'key': key,
                        'value': value,
                        'label': label
                    });
                } else {
                    this.props.checkedObjArr.fetchArray().splice(
                        this.props.checkedObjArr.fetchArray().findIndex(y => y.key == key), 1
                    );
                }
            });    
        } else {
            Alert.alert('Please read Term & Condition before you can tick the checkbox');
        }  
    }

    render() {
        return ( 
            <TouchableHighlight
                onPress={this.stateSwitcher.bind(this, this.props.keyValue, this.props.label, this.props.value)} 
                underlayColor="transparent"
                >

                <View style={{ 
                flexDirection: 'row', 
                alignItems: 'center' }}>
                    <View style={{
                    padding: 4, 
                    width: this.props.size, 
                    height: this.props.size, 
                    backgroundColor: this.props.color
                    }}>
                    {
                        (this.state.checked)
                        ?
                        (<View style={styles.selectedUI}>
                            <Image source={require('../assets/tick.png')} style={styles.checkboxTickImg} />
                        </View>)
                        :
                        (<View style={styles.uncheckedCheckbox} />)
                    }
                </View>
                <Text style={[styles.checkboxLabel, { color: this.props.labelColor }]}>
                    {this.props.label}
                </Text>
                </View>

            </TouchableHighlight>
        );
    }
}

MyCheckBox.propTypes = {
    keyValue: PropTypes.number.isRequired,
    size: PropTypes.number,
    color: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    checked: PropTypes.bool,
    labelColor: PropTypes.string,
    checkedObjArr: PropTypes.object.isRequired
}

MyCheckBox.defaultProps = {
    size: 32,
    checked: false,
    value: 'Default',
    label: 'Default',
    color: '#cecece',
    labelColor: '000000',    
}

const styles = StyleSheet.create( 
{
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },

    selectedUI: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    checkboxTickImg: {
        width: '85%',
        height: '85%',
        tintColor: '#ffffff',
        resizeMode: 'contain'
    },

    uncheckedCheckbox: {
        flex: 1,
        backgroundColor: '#ffffff'
    },

    checkboxLabel: {
        fontSize: 18,
        paddingLeft: 15
    }
});