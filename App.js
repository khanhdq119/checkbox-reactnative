import React, { Component } from 'react';
import { 
  Alert, 
  Image, 
  Platform, 
  StyleSheet, 
  Text, 
  TouchableHighlight, 
  View,
  Linking,
  Button
} from 'react-native';

import TermAndCondition from './components/TermAndCondition';

export default class App extends Component {
  render() {
    return (      
      <View style={styles.container}>
        <TermAndCondition />
      </View>
    );
  }
}
 
const styles = StyleSheet.create( 
  {
    container: {
      flex: 1,
    },
});